.model small
.code
org 100h
start:
	jmp mulai
nama	db 13,10,'Nama Anda     : $'
hp	db 13,10,'No. HP/Telp   : $'                 
lanjut	db 13,10,'LANJUT Tekan y untuk lanjut -> $'
tampung_nama	db 30,?,30 dup(?)
tampung_hp	db 13,?,13 dup(?)
masukkan db 13,10,'Masukkan kode nomor handphone yang ingin anda pilih: $'

daftar  db 13,10,'+.........................................................+'
	    db 13,10,'|                 DAFTAR HANDPHONE                        |'
	    db 13,10,'+.....+...................+...............................+'
	    db 13,10,'|No   | Merk Handphone    | Type          | Harga         |'
	    db 13,10,'+.....+...................+...............+...............+'
	    db 13,10,'|1	| OPPO              | OPPO RENO6    | Rp. 5.199.000 |'
	    db 13,10,'+.....+...................+...............+...............+'
	    db 13,10,'|2	| VIVO              | VIVO V23e     | Rp. 4.399.000 |'
	    db 13,10,'+.....+...................+...............+...............+'
	    db 13,10,'|3	| REALME            | REALME 8      | Rp. 3.599.000 |'
	    db 13,10,'+.....+...................+...............+...............+'
	    db 13,10,'|4	| SAMSUNG           | SAMSUNG A72   | Rp. 5.999.000 |'
	    db 13,10,'+.....+...................+...............+...............+','$'

mulai:
	mov ah,09h
	lea dx,nama
	int 21h
	mov ah,0ah
	lea dx,tampung_nama
	int 21h
	push dx


	mov ah,09h
	lea dx,hp
	int 21h
	mov ah,0ah
	lea dx,tampung_hp
	int 21h
	push dx



	 mov ah,09h
	 mov dx,offset daftar
	 int 21h
	 mov ah,09h
	 mov dx,offset lanjut
	 int 21h
	 mov ah,01h
	 int 21h
	 cmp al,'y'
	 jmp proses
	 jne error_msg
error_msg:
	mov ah,09h
	mov dx,offset error_msg
	int 21h
	int 20h
proses:
	mov ah,09h
	mov dx,offset masukkan
	int 21h
	

	mov ah,01h
	int 21h
	mov bh,al


	cmp al,'1'
	je hasil1

	cmp al,'2'
	je hasil2

	cmp al,'3'
	je hasil3

	cmp al,'4'
	je hasil4

hasil1:
	mov ah,09h
	lea dx,teks1
	int 21h
	int 20h

hasil2:
	mov ah,09h
	lea dx,teks2
	int 21h
	int 20h

hasil3:
	mov ah,09h
	lea dx,teks3
	int 21h
	int 20h
	
hasil4:
	mov ah,09h
	lea dx,teks4
	int 21h
	int 20h

teks1  db 13,10,'Anda memilih OPPO RENO6'
db 13,10,'Total Harga yang anda bayar : Rp. 5.199.000'
db 13,10,'Terima Kasih $'

teks2  db 13,10,'Anda memilih VIVO V23e'
db 13,10,'Total Harga yang anda bayar : Rp. 4.399.000'
db 13,10,'Terima Kasih $'

teks3  db 13,10,'Anda memilih REALME 8'
db 13,10,'Total Harga yang anda bayar : Rp. 3.599.000'
db 13,10,'Terima Kasih $'

teks4  db 13,10,'Anda memilih SAMSUNG A72'
db 13,10,'Total Harga yang anda bayar : Rp. 5.999.000'
db 13,10,'Terima Kasih $'
	

end Start	
